import sys
from typing import Dict, Union
from pathlib import Path, PurePath

from pydantic import BaseSettings


class Settings(BaseSettings):

    API_DIR: Path = PurePath(__file__).parent / "api"
    APP_DIR: Path = PurePath(__file__).parent

    EPSG_SRID_ETRS89: str = "4258"
    EPSG_SRID_ETRS89_LAEA: str = "3035"
    EPSG_SRID_WGS84: str = "4326"
    EPSG_SRID_BELGE_72: str = "4313"
    EPSG_SRID_BELGIAN_LAMBERT_72: str = "31370"

    OPENTELEMETRY_EXPORTER_ZIPKIN_ENDPOINT: str = "http://127.0.0.1:9411/api/v2/spans"
    OPENTELEMETRY_SAMPLING_RATIO: float = 0.1
    OPENTELEMETRY_SERVICE_NAME: str = "addon-nimh-meteorology-weather"

    NIMH_CITIES_LIST_URL: str = "https://users.meteo.bg/MZH/"
    NIMH_CITY_FORECAST_URL: str = "https://users.meteo.bg/MZH/MZHG/{CITY_NAME}.xml"
    NIMH_CONCURRENT_CITIES_DOWNLOAD: int = 5
    NIMH_DATA_TIME_TO_LIVE: int = 30 * 60  # 30 mins
    NIMH_DEFAULT_TIMEOUT: int = 10  # seconds
    NIMH_LOCATION_FORECAST_ORIGIN_NAME: str = "Въз основа на данни от Националния институт по метеорология и хидрология на България"
    NIMH_LOCATION_FORECAST_ORIGIN_URL: str = "https://meteo.bg/"
    NIMH_PASSWORD: str
    NIMH_USERNAME: str
    NIMH_USER_AGENT: str = "FaST Platform https://fastplatform.eu tech.bg@fastplatform.eu"

    LOG_COLORS: bool = sys.stdout.isatty()
    LOG_FORMAT: str = "uvicorn"
    LOG_LEVEL: str = "INFO"

    LOGGING_CONFIG: Dict[str, Union[Dict, bool, int, str]] = {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "simple": {
                "class": "logging.Formatter",
                "format": "%(levelname)-10s %(message)s",
            },
            "uvicorn": {
                "()": "uvicorn.logging.DefaultFormatter",
                "format": "%(levelprefix)s %(message)s",
                "use_colors": LOG_COLORS,
            },
        },
        "handlers": {
            "default": {
                "class": "logging.StreamHandler",
                "formatter": LOG_FORMAT,
                "level": LOG_LEVEL,
                "stream": "ext://sys.stdout",
            }
        },
        "root": {"handlers": ["default"], "level": LOG_LEVEL},
        "loggers": {
            "fastapi": {"propagate": True},
            "uvicorn": {"propagate": True},
            "uvicorn.access": {"propagate": True},
            "uvicorn.asgi": {"propagate": True},
            "uvicorn.error": {"propagate": True},
        },
    }


config = Settings()
