import json
import logging
from shapely.geometry import shape
import graphene
from app.api.types.weather import IntervalType, Sample
from app.lib.gis import Projection
from app.lib.nimh import nimh_client


logger = logging.getLogger(__name__)


class Forecasts:

    # Describe capabilities
    forecasts_implemented = graphene.Boolean(
        default_value=True,
        description="Whether weather forecasts are implemented or not for this provider",
    )
    forecasts_interval_types = graphene.List(
        IntervalType, description="Supported interval types for weather forecasts"
    )

    def resolve_forecasts_interval_types(self, info):
        return [IntervalType.day, IntervalType.hour]

    # Node and resolver for forecasts
    forecasts = graphene.List(
        Sample,
        description="Weather forecasts",
        geometry=graphene.Argument(
            graphene.String, required=True, description="Location being requested"
        ),
        interval_type=graphene.Argument(
            IntervalType, required=True, description="Interval type being requested"
        ),
    )

    async def resolve_forecasts(self, info, geometry, interval_type):
        """Resolver for `forecasts` node

        Arguments:
            info {object} -- GraphQL context
            geometry {str} -- Geometry as a GeoJSON string on ETRS89 datum
            interval_type {enum} -- day/hour

        Returns:
            list -- List of weather Samples
        """
        
        if interval_type not in self.resolve_forecasts_interval_types(info):
            return None

        geometry = shape(json.loads(geometry))
        centroid = geometry.centroid

        samples, closest_city = await nimh_client.get_location_forecast(
            longitude=centroid.x, latitude=centroid.y, interval_type=interval_type
        )

        logger.info(
            f"""{len(samples)} samples returned for location:
            longitude={centroid.x},
            latitude={centroid.y},
            closest_city={closest_city},
            with interval_type={interval_type}
            """
        )

        return samples
