import graphene

from app.api.types.weather import Sample, IntervalType


class Observations:

    # Describe capabilities
    observations_implemented = graphene.Boolean(
        default_value=False,
        description="Whether weather observations are implemented or not for this provider",
    )
    observations_interval_types = graphene.List(
        IntervalType, description="Supported interval types for weather observations"
    )

    def resolve_observations_interval_types(self, info):
        return []

    # Node and resolver for observations
    observations = graphene.List(
        Sample,
        description="Weather (past) observations",
        geometry=graphene.Argument(graphene.String, required=True),
        interval_type=graphene.Argument(IntervalType, required=True),
        date_from=graphene.Argument(graphene.Date, required=False),
        date_to=graphene.Argument(graphene.Date, required=False),
    )

    async def resolve_observations(
        self, info, geometry, interval_type, date_from, date_to
    ):
        """Resolver for `observations` node

        Arguments:
            info {object} -- GraphQL context
            geometry {str} -- Geometry as a GeoJSON string
            date_from {date} -- [description]
            date_to {str} -- [description]

        Returns:
            list -- List of weather Samples
        """
        
        return []
