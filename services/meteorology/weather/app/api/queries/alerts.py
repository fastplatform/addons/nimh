import graphene

from app.api.types.weather import Alert


class Alerts:

    # Describe capabilities
    alerts_implemented = graphene.Boolean(
        default_value=False,
        description="Whether weather alerts are implemented or not for this provider",
    )

    # Node and resolver for alerts
    alerts = graphene.List(
        Alert,
        description="Weather alerts",
        geometry=graphene.Argument(
            graphene.String,
            required=True,
            description="Weather alerts",
        ),
    )

    def resolve_alerts(self, info, geometry):
        """Resolver for `alerts` node

        Arguments:
            info {object} -- GraphQL context
            geometry {str} -- Geometry as a GeoJSON string on ETRS89 datum

        Returns:
            list -- List of Alerts
        """

        return []
