import logging
import re

from typing import Optional, List
from enum import Enum
from datetime import datetime
from pydantic import BaseModel, Field, confloat, validator


logger = logging.getLogger(__name__)

r_nullable_time = re.compile(r"^(([0-1]?[0-9]|2[0-3]):[0-5][0-9]|)$")


def na_to_none(value):
    if value == "NA":
        return None
    return value


class InvalidTimeException(Exception):
    """Custom error that is raised when time is not of format HH:mm [0-23]:[0-59]"""

    def __init__(self, title: str, message: str) -> None:
        self.title = title
        self.message = message
        super().__init__(message)


def is_nullable_time(value):
    if value == "-----":
        value = ""

    if not bool(r_nullable_time.match(value)):
        raise InvalidTimeException(
            title="InvalidTimeException",
            message=f"Value {value} does not validate format HH:mm [0-23]:[0-59]",
        )
    return value


class NimhForecastTemperatureDetails(BaseModel):
    id: Optional[str] = None
    unit: Optional[str] = None
    value: Optional[int] = None

    @validator("value", pre=True)
    def na_to_none(cls, value):
        return na_to_none(value)


class NimhForecastWindDirectionDetails(BaseModel):
    id: Optional[str] = None
    deg: Optional[int] = None
    name: Optional[str] = None

    @validator("deg", pre=True)
    def na_to_none(cls, value):
        return na_to_none(value)


class NimhForecastWindSpeedDetails(BaseModel):
    id: Optional[str] = None
    mps: Optional[float] = None
    beaufort: Optional[str] = None
    name: Optional[str] = None

    @validator("mps", pre=True)
    def na_to_none(cls, value):
        return na_to_none(value)


class NimhForecastHumidityDetails(BaseModel):
    value: Optional[int] = None
    unit: Optional[str] = None

    @validator("value", pre=True)
    def na_to_none(cls, value):
        return na_to_none(value)


class NimhForecastPressureDetails(BaseModel):
    id: Optional[str] = None
    unit: Optional[str] = None
    value: Optional[int] = None

    @validator("value", pre=True)
    def na_to_none(cls, value):
        return na_to_none(value)


class NimhForecastCloudDetails(BaseModel):
    id: Optional[str] = None
    percent: Optional[int] = None

    @validator("percent", pre=True)
    def na_to_none(cls, value):
        return na_to_none(value)


class NimhForecastPrecipitationDetails(BaseModel):
    unit: Optional[str] = None
    value: Optional[float] = None

    @validator("value", pre=True)
    def na_to_none(cls, value):
        return na_to_none(value)


class NimhForecastSymbolDetails(BaseModel):
    id: Optional[str] = None
    number: Optional[str] = None


class NimhForecastTempIndexDetails(BaseModel):
    comfort: Optional[float] = None
    feeling: Optional[float] = None

    @validator("comfort", "feeling", pre=True)
    def na_to_none(cls, value):
        return na_to_none(value)


class NimhForecastProbRainDetails(BaseModel):
    value: Optional[str] = None
    unit: Optional[str] = None

    @validator("value", pre=True)
    def na_to_none(cls, value):
        return na_to_none(value)


class NimhForecastMoonDetails(BaseModel):
    rise: Optional[str] = None
    set: Optional[str] = None
    illumination: Optional[float] = None
    daysto: Optional[int] = None
    phase: Optional[str] = None

    @validator("rise", "set", pre=True)
    def is_time(cls, value):
        return is_nullable_time(value)


class NimhForecastSunDetails(BaseModel):
    rise: Optional[str] = None
    set: Optional[str] = None
    daylength: Optional[str] = None

    @validator("rise", "set", pre=True)
    def is_time(cls, value):
        return is_nullable_time(value)


class NimhForecastTimeSeriesLocation(BaseModel):
    altitude: Optional[int] = None
    latitude: Optional[float] = None
    longitude: Optional[float] = None
    id: Optional[int] = None
    name: Optional[str] = None
    temperature: Optional[NimhForecastTemperatureDetails] = None
    wind_direction: Optional[NimhForecastWindDirectionDetails] = Field(
        None, alias="windDirection"
    )
    wind_speed: Optional[NimhForecastWindSpeedDetails] = Field(None, alias="windSpeed")
    humidity: Optional[NimhForecastHumidityDetails] = None
    pressure: Optional[NimhForecastPressureDetails] = None
    cloudiness: Optional[NimhForecastCloudDetails] = None

    fog: Optional[NimhForecastCloudDetails] = None
    low_clouds: Optional[NimhForecastCloudDetails] = Field(None, alias="lowClouds")
    medium_clouds: Optional[NimhForecastCloudDetails] = Field(
        None, alias="mediumClouds"
    )
    high_clouds: Optional[NimhForecastCloudDetails] = Field(None, alias="highClouds")
    precipitation: Optional[NimhForecastPrecipitationDetails] = None
    symbol: Optional[NimhForecastSymbolDetails] = None
    temp_index: Optional[NimhForecastTempIndexDetails] = Field(None, alias="tempIndex")
    prob_rain: Optional[NimhForecastProbRainDetails] = Field(None, alias="probRain")
    min_daily_tempature: Optional[NimhForecastTemperatureDetails] = Field(
        None, alias="min.daily.tempature"
    )
    max_daily_tempature: Optional[NimhForecastTemperatureDetails] = Field(
        None, alias="max.daily.tempature"
    )
    sun: Optional[NimhForecastSunDetails] = Field(None, alias="Sun")
    moon: Optional[NimhForecastMoonDetails] = Field(None, alias="Moon")


class NimhForecastTimeSeries(BaseModel):
    datatype: Optional[str] = None
    time_from: Optional[datetime] = Field(None, alias="from")
    to: Optional[datetime] = None
    location: Optional[NimhForecastTimeSeriesLocation] = None


class NimhForecastProduct(BaseModel):
    product_class: Optional[str] = Field(None, alias="class")
    time_series: List[NimhForecastTimeSeries] = Field(alias="time")


class NimhForecastWeatherdata(BaseModel):
    created: datetime
    products: List[NimhForecastProduct] = Field(alias="product")


class NimhForecastResponse(BaseModel):
    weatherdata: NimhForecastWeatherdata
