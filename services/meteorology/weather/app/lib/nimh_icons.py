# see docs/api-specs.md#List of nimh_icon_number from meteo.bg for more info
_nimh_icons_mapping = {
    "01": "clear_day",
    "02": "partly_cloudy_day",
    "03": "partly_cloudy_day",
    "04": "partly_cloudy_day",
    "05": "cloudy",
    "06": "light_rain",
    "07": "light_snow",
    "08": "clear_day",
    "09": "cloudy",
    "10": "rain",
    "11": "light_rain",
    "12": "thunderstorm",
    "13": "snow",
    "14": "light_snow",
    "15": "snow",
    "16": "fog",
    "17": "light_rain",
    "18": "partly_cloudy_day",
    "19": "light_rain",
    "20": "thunderstorm",
}

# As a reminder, here are the weather icons in fast ontology:
# class Icon(graphene.Enum):
#     clear_day = "clear_day"
#     clear_night = "clear_night"
#     cloudy = "cloudy"
#     fog = "fog"
#     hail = "hail"
#     light_rain = "light_rain"
#     light_snow = "light_snow"
#     partly_cloudy_day = "partly_cloudy_day"
#     partly_cloudy_night = "partly_cloudy_night"
#     rain = "rain"
#     snow = "snow"
#     thunderstorm = "thunderstorm"
#     wind = "wind"
#     ice = "ice"
#     other = "other"


def get_fastplatform_icon_from_nimh_icon_number(nimh_icon_number):
    formatted_nimh_icon_number = str(nimh_icon_number).zfill(2)
    if formatted_nimh_icon_number in _nimh_icons_mapping:
        return _nimh_icons_mapping[formatted_nimh_icon_number]

    return None
