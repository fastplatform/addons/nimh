import asyncio
import hashlib
import logging
import json
import pytz

from datetime import datetime
import httpx
from graphql import GraphQLError
from bs4 import BeautifulSoup

import xmltodict


from app.lib.nimh_datatypes import NimhForecastResponse
from app.lib.nimh_icons import get_fastplatform_icon_from_nimh_icon_number
from app.settings import config
from app.api.types.weather import (
    Sample,
    SampleType,
    IntervalType,
    Location,
    PrecipitationType,
    Origin,
    Icon,
)


logger = logging.getLogger(__name__)
TZ = pytz.timezone("Europe/Sofia")


class NimhClient:
    """A client to retrieve data from Nimh API"""

    def __init__(self):
        self.http_client = None
        self.cities_live_samples = {}
        self.cities_coords = {}

    def create_http_client(self):
        self.http_client = httpx.AsyncClient()

    async def close_http_client(self):
        await self.http_client.aclose()

    async def get_location_forecast(self, longitude, latitude, interval_type):
        """Find the closest city to location provided, refresh data if necessary and return it.

        Args:
            longitude (float): Longitude in degrees (lat/long inverted because we expect this order: x/y)
            latitude (float): Latitude in degrees
            interval_type ([type]): hour/day

        Returns:
            [Sample]: the weather forecast from the closest known location
        """

        # Check we have cities coordinates
        if not self.cities_coords:
            await self.refresh_all_cities()

        # Get nearest city (ugly distance computing)
        distances = {}
        for city in self.cities_coords:
            distances[city] = (
                (self.cities_coords[city]["latitude"] - latitude) ** 2
                + (self.cities_coords[city]["longitude"] - longitude) ** 2,
            )

        city, _ = min(distances.items(), key=lambda x: x[1])

        # Get live sample
        city_data = self.cities_live_samples[city]
        last_computed_at = city_data["computed_at"]

        # Refresh city data if necessary
        if (
            datetime.now(tz=TZ) - last_computed_at
        ).total_seconds() > config.NIMH_DATA_TIME_TO_LIVE:
            await self.refresh_city(
                city, config.NIMH_CITY_FORECAST_URL.format(CITY_NAME=city)
            )

        # Return samples filtered by interval type
        samples = [
            sample
            for sample in city_data["samples"]
            if sample.interval_type == interval_type
        ]

        return samples, city

    async def refresh_all_cities(self):
        """Iterate through the list of cities provided as envvar and refresh forecast"""

        try:
            response = await self.http_client.get(
                config.NIMH_CITIES_LIST_URL,
                params={},
                timeout=config.NIMH_DEFAULT_TIMEOUT,
                auth=(config.NIMH_USERNAME, config.NIMH_PASSWORD),
            )
            response.raise_for_status()

            content = response.content
            bs_content = BeautifulSoup(content, features="html.parser")

            rows = bs_content.find_all("a")
            cities = [str(row.contents[0]).replace(".xml", "") for row in rows]

            if not cities:
                raise Exception("No cities were found on the cities list page")

        except Exception as exception:
            logger.exception(
                "Failed to retrieve list of cities from %s", config.NIMH_CITIES_LIST_URL
            )
            raise GraphQLError("API_REMOTE_ERROR") from exception

        semaphore = asyncio.Semaphore(config.NIMH_CONCURRENT_CITIES_DOWNLOAD)

        async def download(city):
            async with semaphore:
                await self.refresh_city(
                    city,
                    config.NIMH_CITY_FORECAST_URL.format(CITY_NAME=city),
                )

        downloads = [download(city=city) for city in cities]

        await asyncio.gather(*downloads)

    async def refresh_city(self, city, city_url):
        """Get the latest available forecast for a given city

        Args:
            city (str): The name of the city as found in config.NIMH_CITIES_LIST_URL
            city_url (str): The url queried to get latest forecast
        """
        try:
            response = await self.http_client.get(
                city_url,
                params={},
                timeout=config.NIMH_DEFAULT_TIMEOUT,
                auth=(config.NIMH_USERNAME, config.NIMH_PASSWORD),
            )
            response.raise_for_status()

            content = response.content
        except Exception as exception:
            logger.exception("Failed to retrieve forecasts")
            raise GraphQLError("API_REMOTE_ERROR") from exception

        xml_content = xmltodict.parse(
            content,
            attr_prefix="",
            process_namespaces=False,
            encoding="utf-8",
        )

        js_dumps = json.dumps(xml_content, ensure_ascii=False).encode("utf8")
        response = json.loads(js_dumps)
        computed_at, samples = self.parse_location_forecast_response(city, response)

        self.cities_live_samples[city] = {
            "computed_at": computed_at,
            "samples": samples,
        }

    def parse_location_forecast_response(self, city, response):
        """Parse the response from the Nimh API into the FaST ontology

        Args:
            city (str): The city which forecast we are parsing
            response (dict): Dict/JSON response from the API

        Raises:
            GraphQLError:
                - API_DATA_ERROR: data parsing error

        Returns:
            [Sample]: List of Sample GraphQL nodes
        """
        # Parse the response body
        try:
            data = NimhForecastResponse(**response)

        except Exception as ex:
            logger.exception("Failed to parse forecasts")
            raise GraphQLError("API_DATA_ERROR") from ex

        # Parse the headers
        computed_at = TZ.localize(data.weatherdata.created, is_dst=True)

        # Convert from Nimh data model to FaST Ontology
        samples = []
        for product in data.weatherdata.products:
            if product.product_class == "pointData":
                interval_type = IntervalType.hour
            elif product.product_class == "pointDataDaily":
                interval_type = IntervalType.day
            else:
                interval_type = None

            for time_series_element in product.time_series:

                location_element = time_series_element.location

                time_from = TZ.localize(time_series_element.time_from, is_dst=True)
                time_to = TZ.localize(time_series_element.to, is_dst=True)

                try:
                    nimh_icon_number = location_element.symbol.number
                    icon = (
                        get_fastplatform_icon_from_nimh_icon_number(nimh_icon_number)
                        if nimh_icon_number is not None
                        else None
                    )
                except AttributeError:
                    nimh_icon_number = None
                    icon = None

                try:
                    precipitation_value = location_element.precipitation.value

                    # Try to infer the precipitation type from the Nimh/FaST symbol
                    if precipitation_value is None:
                        precipitation_type = None
                    if precipitation_value > 0 and nimh_icon_number is not None:
                        if icon == Icon.thunderstorm:
                            precipitation_type = PrecipitationType.storm
                        elif icon in [Icon.rain, Icon.light_rain]:
                            precipitation_type = PrecipitationType.rain
                        elif icon in [Icon.snow, Icon.light_snow]:
                            precipitation_type = PrecipitationType.snow
                        elif icon == Icon.hail:
                            precipitation_type = PrecipitationType.hail
                        else:
                            precipitation_type = PrecipitationType.rain
                    else:
                        precipitation_type = None
                except AttributeError:
                    precipitation_value = None
                    precipitation_type = None

                min_daily_temperature = (
                    location_element.min_daily_tempature.value
                    if location_element.min_daily_tempature
                    else None
                )
                max_daily_temperature = (
                    location_element.max_daily_tempature.value
                    if location_element.max_daily_tempature
                    else None
                )
                wind_direction = (
                    location_element.wind_direction.deg
                    if location_element.wind_direction
                    else None
                )
                cloud_cover = (
                    location_element.cloudiness.percent
                    if location_element.cloudiness
                    else None
                )

                precipitation_probability = (
                    float(location_element.prob_rain.value or 0) / 100
                    if location_element.prob_rain
                    else None
                )
                temperature = (
                    location_element.temperature.value
                    if location_element.temperature
                    else None
                )
                humidity = (
                    location_element.humidity.value
                    if location_element.humidity
                    else None
                )
                pressure = (
                    location_element.pressure.value
                    if location_element.pressure
                    else None
                )

                wind_speed = (
                    location_element.wind_speed.mps
                    if location_element.wind_speed
                    else None
                )

                samples.append(
                    Sample(
                        sample_type=SampleType.forecast,
                        interval_type=interval_type,
                        location=Location(
                            authority=None,
                            authority_id=None,
                            geometry={
                                "type": "Point",
                                "coordinates": (
                                    location_element.latitude,
                                    location_element.longitude,
                                ),
                            },
                            name=location_element.name,
                        ),
                        valid_from=time_from,
                        valid_to=time_to,
                        computed_at=computed_at,
                        fetched_at=None,
                        origin=Origin(
                            name=config.NIMH_LOCATION_FORECAST_ORIGIN_NAME,
                            url=config.NIMH_LOCATION_FORECAST_ORIGIN_URL,
                            icon=None,
                        ),
                        summary=None,
                        icon=icon,
                        nearest_storm_distance=None,
                        nearest_storm_bearing=None,
                        precipitation_intensity=precipitation_value,
                        precipitation_intensity_error=None,
                        precipitation_probability=precipitation_probability,
                        precipitation_type=precipitation_type,
                        temperature=temperature,
                        temperature_min=min_daily_temperature,
                        temperature_max=max_daily_temperature,
                        temperature_min_at=None,
                        temperature_max_at=None,
                        apparent_temperature=None,
                        dew_point=None,
                        humidity=humidity,
                        humidity_min=None,
                        humidity_max=None,
                        humidity_min_at=None,
                        humidity_max_at=None,
                        pressure=pressure,
                        wind_speed=wind_speed,
                        wind_speed_max=None,
                        wind_speed_max_at=None,
                        wind_speed_max_bearing=None,
                        wind_gust=None,
                        wind_bearing=wind_direction,
                        cloud_cover=cloud_cover,
                        uv_index=None,
                        radiation=None,
                        visibility=None,
                        ozone=None,
                        soil_temperatures=None,
                        evapotranspiration=None,
                        source=time_series_element.json(),
                    )
                )

                # Make sure city exists in dict
                if not city in self.cities_coords.keys():
                    self.cities_coords[city] = {
                        "latitude": location_element.latitude,
                        "longitude": location_element.longitude,
                    }

        return computed_at, samples


nimh_client = NimhClient()
