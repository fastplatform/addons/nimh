# API specs

## List of nimh_icon_number from meteo.bg

- See [html](legend-meteo-bg.html) from [meteo.bg](http://meteo.bg/en/forecast/dnesUtre)

| icon_number | meteo.bg                                   | fast ontology     |
| :---------: | ------------------------------------------ | ----------------- |
|     01      | Clear/Sunny                                | clear_day         |
|     02      | Variable clouds                            | partly_cloudy_day |
|     03      | Variable clouds, possible rain             | partly_cloudy_day |
|     04      | Variable clouds, possible snow             | partly_cloudy_day |
|     05      | Mostly cloudy                              | cloudy            |
|     06      | Mostly cloudy with rain                    | light_rain        |
|     07      | Mostly cloudy with snow                    | light_snow        |
|     08      | Mostly sunny                               | clear_day         |
|     09      | Overcast                                   | cloudy            |
|     10      | Rain                                       | rain              |
|     11      | Short rain                                 | light_rain        |
|     12      | Thunder and rain                           | thunderstorm      |
|     13      | Rain & snow                                | snow              |
|     14      | Weak snow                                  | light_snow        |
|     15      | Heavy snowfall                             | snow              |
|     16      | Fog                                        | fog               |
|     17      | Variable - sun, cloud, shower, and thunder | light_rain        |
|     18      | Sunny spells                               | partly_cloudy_day |
|     19      | Weak rain                                  | light_rain        |
|     20      | Thunderstorm                               | thunderstorm      |

### Comparison between meteo.bg and weather.bg

See [html](legend-weather-bg.html) from [weather.bg](https://weather.bg/0index.php?lng=1) (not used here)

| icon_number | weather.bg (NOT USED)                                                | meteo.bg                                   | fast ontology     |
| :---------: | -------------------------------------------------------------------- | ------------------------------------------ | ----------------- |
|     01      | clear/sunny                                                          | Clear/Sunny                                | clear_day         |
|     02      | partly cloudy                                                        | Variable clouds                            | partly_cloudy_day |
|     03      | mostly cloudy                                                        | Variable clouds, possible rain             | partly_cloudy_day |
|     04      | visibility reduced by smoke,dust or sand;dust or sand whirl(s);mist; | Variable clouds, possible snow             | partly_cloudy_day |
|     05      | duststorm or sandstorm; thick fog;                                   | Mostly cloudy                              | cloudy            |
|     06      |                                                                      | Mostly cloudy with rain                    | light_rain        |
|     07      |                                                                      | Mostly cloudy with snow                    | light_snow        |
|     08      |                                                                      | Mostly sunny                               | clear_day         |
|     09      | haze; shallow fog;                                                   | Overcast                                   | cloudy            |
|     10      |                                                                      | Rain                                       | rain              |
|     11      |                                                                      | Short rain                                 | light_rain        |
|     12      |                                                                      | Thunder and rain                           | thunderstorm      |
|     13      | lightening(s)                                                        | Rain & snow                                | snow              |
|     14      | rain shower, not falling; cloudy                                     | Weak snow                                  | light_snow        |
|     15      | shower near the station                                              | Heavy snowfall                             | snow              |
|     16      |                                                                      | Fog                                        | fog               |
|     17      |                                                                      | Variable - sun, cloud, shower, and thunder |                   |
|     18      | squall(s); tornado; recent thunder;                                  | Sunny spells                               | partly_cloudy_day |
|     19      | recent drizzle                                                       | Weak rain                                  | light_rain        |
|     20      | drizzle and rain; recent rain; slight rain shower(s);                | Thunderstorm                               | thunderstorm      |
|     21      | recent snow; light snow; snow or crystals                            |                                            | light_snow        |
|     22      | rain and snow                                                        |                                            | light_snow        |
|     23      | freezing drizzle/rain                                                |                                            | ice               |
|     24      | ?                                                                    |                                            | light_rain        |
|     25      | recent snow shower;light snow;                                       |                                            | light_snow        |
|     26      | recent hail; sleet; hail;                                            |                                            | hail              |
|     27      | blowing snow; moderate snow; snow shower(s);                         |                                            | snow              |
|     28      | light or dissipating fog;                                            |                                            | fog               |
|     29      | light or moderate drizzle;                                           |                                            | light_rain        |
|     30      | heavy drizzle                                                        |                                            | rain              |
|     31      | moderate rain;                                                       |                                            | rain              |
|     32      | moderate rain; moderate rain shower(s);                              |                                            | rain              |
|     33      | heavy rain                                                           |                                            | rain              |
|     34      | violent rain shower(s)                                               |                                            | rain              |
|     35      | rain and snow                                                        |                                            | snow              |
|     36      | heavy snow; snow shower(s)                                           |                                            | snow              |
|     37      | diamond dust,sleet                                                   |                                            | snow              |
|     38      | heavy rain and snow                                                  |                                            | snow              |
|     39      | hailstorm                                                            |                                            | hail              |
|             |

## Coordinate system

The SRID is WGS84 (EPSG:4326) as most ECMWF products. No reprojection applied.

## Datetime localization

tl;dr `2021-10-28 22:16:17` becomes `2021-10-28 22:16:17+03:00` 


Dates received from NIMH are all `naive` (no timezone information) Bulgarian datetimes (`timezone=Europe/Sofia`).
Example: `2021-10-28 22:16:17`.

What we return is a datetime that is `offset aware`, and take into account Daylight Saving Time (`is_dst=True`).

[nimh.py](../app/lib/nimh.py):
```py
def parse_location_forecast_response(self, city, response):
    data = NimhForecastResponse(**response)

    computed_at = TZ.localize(data.weatherdata.created, is_dst=True)

    # Let's print value before and after localization:
    print(data.weatherdata.created)
    >> 2021-10-28 22:16:17

    print(computed_at)
    >> 2021-10-28 22:16:17+03:00
```

## precipitation_probability

In the example provided, we always get <probRain value="NA" unit="percent"/>

We presume values are between 0 and 100, and return a value between 0 and 100 (like on other services).

`NA` -> `0`

## cloud_cover

Example: <cloudiness id="NN" percent="18"/> 
-> We return a values between 0 and 100 (like other on other services).