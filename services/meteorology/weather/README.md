# `nimh`: NIMH weather service for Bulgaria

This service exposes weather forecasts to the FaST users and is based on public data exposed by the National Institute of Meteorology and Hydrology of Bulgaria at [meteo.bg](http://meteo.bg).

_This service and its implementation are not in any way endorsed by The National Institute of Meteorology and Hydrology._
## Architecture

The service is composed of a GraphQL server that exposes a schema compliant with the *FaST Weather GraphQL ontology*. The GraphQL server is implemented using the [Graphene](https://graphene-python.org/) framework + [fastapi](https://fastapi.tiangolo.com/)/[Starlette](https://www.starlette.io/) and served behind a [uvicorn](https://www.uvicorn.org/) server.

```plantuml

agent "Farmer app" as farmer
component "FaST API Gateway" as api #line.dashed
component "Web server" as web {
    storage "In-memory\ncache" as cache
}
cloud {
    folder "NIMH forecast repository" as nimh_repo {
        file "city_1.xml"
        file "city_2.xml"
        file "..."
        file "city_N.xml"
    }
}

farmer --> api      : /graphql
api --> web         : /graphql
web --> nimh_repo   : Download with\nHTTP Basic Auth

```

#### How it works

1. The client provides a geometry that is converted to a set of latitude/longitude coordinates.
2. If the service does not yet have in its cache the forecast for all the cities listed in `NIMH_CITIES`, or if the cached forecasts have expired (`NIMH_DATA_TIME_TO_LIVE`), the forecast data is downloaded from the NIMH repository.
3. The service finds the city closest to the requested latitude/longitude.
4. The service returns the forecast for that city.

## GraphQL server

The service exposes a GraphQL server on the `/graphql` endpoint. The following (high-level) GraphQL schema is exposed:

```graphql
query {
    weather {
        # Weather forecasts (future)
        forecasts_implemented
        forecasts_interval_types
        forecast(geometry, interval_type) {
            id
            sample_type
            interval_type
            location {
                id
                authority
                authority_id
                geometry
                name
            }
            valid_from
            valid_to
            computed_at
            fetched_at
            origin {
                id
                name
                url
                icon
            }
            summary
            icon
            nearest_storm_distance
            nearest_storm_bearing
            precipitation_intensity
            precipitation_intensity_error
            precipitation_probability
            precipitation_type
            temperature
            temperature_min
            temperature_max
            temperature_min_at
            temperature_max_at
            apparent_temperature
            dew_point
            humidity
            humidity_min
            humidity_max
            humidity_min_at
            humidity_max_at
            pressure
            wind_speed
            wind_speed_max
            wind_speed_max_at
            wind_speed_max_bearing
            wind_gust
            wind_bearing
            cloud_cover
            uv_index
            radiation
            visibility
            ozone
            soil_temperatures {
                id
                depth
                temperature
            }
            evapotranspiration
            source
        }

        # Weather observations (past)
        observations_implemented  # = false
        observations_interval_types
        observations(geometry, interval_type, date_from, date_to) {
            # ... same fields as the `forecast` node
        }
        
        # Weather alerts
        alerts_implemented  # = false
        alerts(geometry) {
            id
            sender
            issued_at
            title
            summary
            description
            url
            category
            urgency
            severity
            certainty
            location
            valid_from
            valid_to
        }
    }
}

# The service does not expose any mutation or subscription.
```

The schema is self documenting and we encourage the reader to introspect it to learn more about each node and its associated fields and relations. This can done using the [GraphiQL](https://github.com/graphql/graphiql) or [Altair](https://altair.sirmuel.design/) tools, for example.

<div align="center">
    <img src="docs/img/graphql-schema.png" width="420">
</div>

A sample query is provided, as part of the test suite: [app/tests/]().

## Environment variables

Below are the available environment variables for the web server:
- `NIMH_CITIES_LIST_URL`: URL of the current list of cities (defaults to https://users.meteo.bg/MZH/)
- `NIMH_CITY_FORECAST_URL`: URL of the forecast file for city `CITY_NAME` (defaults to https://users.meteo.bg/MZH/MZHG/{CITY_NAME}.xml)
- `NIMH_CONCURRENT_CITIES_DOWNLOAD`: Number of concurrent city forecasts to download from the NIMH repository
- `NIMH_DATA_TIME_TO_LIVE`: The timespan in seconds of a city meteorological data, i.e. the time after which data needs to be refreshed from the server (defaults to `1800`, i.e. 30 minutes)
- `NIMH_DEFAULT_TIMEOUT`: Timeout of downloads from the NIMH repository (defaults to `10` seconds)
- `NIMH_LOCATION_FORECAST_ORIGIN_NAME`: Origin name that will be inserted in the GraphQL response (defaults to `Based on data from NIMH Bulgaria` in Bulgarian)
- `NIMH_LOCATION_FORECAST_ORIGIN_URL`: Origin URL that will be inserted in the GraphQL response (defaults to `https://meteo.bg/`)
- `NIMH_PASSWORD`: The password provided by NIMH to access forecast data
- `NIMH_USERNAME`: The username provided by NIMH to access forecast data
- `NIMH_USER_AGENT`: User-Agent header that will be sent to the NIMH API with every request, to identify the FaST backend.

## Development 

To run the service on your local machine, ensure you have the following dependencies installed:
- [Python 3.7+](https://www.python.org/) with the `virtualenv` package installed
- The `make` utility

Create a virtual environment and activate it:
```bash
virtualenv .venv
source .venv/bin/activate
```

Install the Python dependencies:
```bash
pip install -r requirements.txt
pip install -r requirements-dev.txt  # optional
```

Start the development server with auto-reload enabled:
```bash
make start
```

The GraphQL endpoint is now available at `http://localhost:7777/graphql`.

### Management commands

The following management commands are available from the `Makefile`.

- Uvicorn targets:
    - `make start`: Start the development server with auto-reload enabled.

- Testing targets:
    - `make test`: Run the test query
	
Some additional commands are available in the `Makefile`, we encourage you to read it (or to type `make help` to get the full list of commands).

## Running the tests

Start the server (if it is not already running):
```bash
make start
```

In another shell, run the test query:
```bash
make test
```

*City coordinates for manual tests*

Here are a few cities far apart in Bulgaria.

Replace at the appropriate place in [variables](../app/tests/variables.json) and run `make test` again.

| Coordinates          | City        | Where on the map |
| -------------------- | ----------- | :--------------: |
| `[22.8679, 43.9962]` | Vidin       |        NW        |
| `[23.3219, 42.6977]` | Sofia       |        W         |
| `[23.0943, 42.0209]` | Blagoevgrad |        SW        |
| `[24.7453, 42.1354]` | Plovdiv     |        S         |
| `[27.4626, 42.5048]` | Burgas      |        SE        |
| `[26.9361, 43.2712]` | Shumen      |        NE        |
| `[24.6067, 43.4170]` | Pleven      |        N         |
