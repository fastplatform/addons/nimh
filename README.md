# `nimh`: NIMH weather service for Bulgaria

This service exposes climatic and weather observations and forecasts to the Bulgarian users of FaST, based on the National Institute of Meteorology and Hydrology data and APIs.

As an additional module of the FaST Platform, it integrates the main service(s) necessary for its operation as well as any other ancillary systems such as databases or cache systems. This repo includes both the [source code](services) and [a descriptive orchestration configuration](manifests) of the entire package.

Services:
* [meteorology/weather](services/meteorology/weather)
