#!/bin/bash


set -o errexit
set -o nounset
set -o pipefail

source $(dirname $0)/ensure-gnu-xargs.sh
source $(dirname $0)/ensure-buildozer.sh

SERVICES_DIR="services"

CUSTOM_SERVICES=$(find $SERVICES_DIR -maxdepth 2 -mindepth 2 -type f -name BUILD | xargs -n 1 -r dirname)

buildozer 'remove srcs' //:images_manifest 2>/dev/null || true
buildozer 'remove srcs' //manifests:services_graphql_queries 2>/dev/null || true
buildozer 'remove srcs' //manifests:services_kustomize_resources 2>/dev/null || true

for cs in $CUSTOM_SERVICES; do
  scs=$(find $cs -maxdepth 2 -mindepth 2 -type f -name BUILD | xargs -n 1 -r sh -c 'basename $(dirname $0)')
  scs_images_attr=$(echo $scs | xargs -n 1 -r sh -c 'echo {STABLE_DOCKER_REGISTRY}/{STABLE_DOCKER_IMAGE_PREFIX}$(echo $(basename '$cs')/$0 | sed 's/_/-/g'){STABLE_NIMH_TAG_PREFIXED_WITH_COLON}://'$cs'/$0:image')

  buildozer 'new_load //tools/macros:image.bzl image_bundle' //$cs:__pkg__ 2>/dev/null|| true

  buildozer \
    'add srcs //'$cs:image_bundle_manifest \
    'remove_comment srcs' \
    'comment srcs ATTRIBUTE\ AUTOMATICALLY\ GENERATED\ DO\ NOT\ EDIT!' \
    //:images_manifest

  buildozer \
    'add srcs //'$cs:kustomize_resources \
    'remove_comment srcs' \
    'comment srcs ATTRIBUTE\ AUTOMATICALLY\ GENERATED\ DO\ NOT\ EDIT!' \
    //manifests:services_kustomize_resources

  buildozer delete //$cs:image_bundle 2>/dev/null|| true
  buildozer 'new image_bundle image_bundle' //$cs:__pkg__
  buildozer \
    'comment RULE\ AUTOMATICALLY\ GENERATED\ DO\ NOT\ EDIT!' \
    'set format "Docker"' \
    'add visibility //:__pkg__' \
    //$cs:image_bundle || (buildozer delete //$cs:image_bundle && exit 1)
  if [[ -z "$scs_images_attr" ]]; then
    buildozer \
      'set images {}' \
      'comment images NO\ SUBPACKAGE\ FOUND!' \
      //$cs:image_bundle
  else 
    buildozer "dict_add images $scs_images_attr" //$cs:image_bundle
  fi

  buildozer delete //$cs:kustomize_resources 2>/dev/null || true
  buildozer delete //$cs:graphql_queries  2>/dev/null || true

  buildozer 'new filegroup kustomize_resources' //$cs:__pkg__
  buildozer \
    'comment RULE\ AUTOMATICALLY\ GENERATED\ DO\ NOT\ EDIT!' \
    'add visibility //manifests:__pkg__' \
    //$cs:kustomize_resources || (buildozer delete //$cs:kustomize_resources && exit 1)

  buildozer 'new filegroup graphql_queries' //$cs:__pkg__
  buildozer \
    'comment RULE\ AUTOMATICALLY\ GENERATED\ DO\ NOT\ EDIT!' \
    'add visibility //manifests:__pkg__' \
    //$cs:graphql_queries || (buildozer delete //$cs:graphql_queries && exit 1)

  if [[ ! -z "$scs" ]]; then
    graphql_queries_detected=0
    for s in $scs; do
      buildozer 'add srcs //'$cs/$s':kustomize_resources' //$cs:kustomize_resources
      buildozer delete //$cs/$s:graphql_queries 2>/dev/null || true
      if (( $(find $cs/$s -type f -name "*.graphql" -or -name "*.gql" | wc -l) > 0 )); then
        graphql_queries_detected=1
        buildozer 'new filegroup graphql_queries' //$cs/$s:__pkg__
        buildozer \
          'set srcs glob(["**/*.gql","**/*.graphql"])' \
          'comment RULE\ AUTOMATICALLY\ GENERATED\ DO\ NOT\ EDIT!' \
          //$cs/$s:graphql_queries || (buildozer delete //$cs/$s:graphql_queries && exit 1)
        buildozer 'add srcs //'$cs/$s':graphql_queries' //$cs:graphql_queries
      fi
    done
    if (( $graphql_queries_detected )); then
      buildozer \
        'add srcs //'$cs:graphql_queries \
        'remove_comment srcs' \
        'comment srcs ATTRIBUTE\ AUTOMATICALLY\ GENERATED\ DO\ NOT\ EDIT!' \
        //manifests:services_graphql_queries
    else
      buildozer delete //$cs:graphql_queries  2>/dev/null || true
      buildozer \
        'add srcs BUILD' \
        'remove_comment srcs' \
        'comment srcs ATTRIBUTE\ AUTOMATICALLY\ GENERATED\ DO\ NOT\ EDIT!\ NO\ GRAPHQL\ FILE\ DETECTED,\ BY\ DEFAULT\ "SRCS"\ ATTRIBUTE\ IS\ SET\ TO\ "BUILD"\ TO\ NOT\ LEAVE\ IT\ EMPTY.' \
        //manifests:services_graphql_queries
    fi
  fi

done